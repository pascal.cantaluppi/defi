const Reward = artifacts.require('Reward');
const Tether = artifacts.require('Tether');
const Bank = artifacts.require('Bank');

module.exports = async function (deployer, network, accounts) {
  // Deploy mock tether token
  await deployer.deploy(Tether, { gas: 10000000 });
  const tether = await Tether.deployed();

  // Deploy reward token
  await deployer.deploy(Reward, { gas: 10000000 });
  const reward = await Reward.deployed();

  // Deploy decentralBank
  await deployer.deploy(Bank, reward.address, tether.address, { gas: 10000000 });
  const bank = await Bank.deployed();

  // Transfer all tokens to decentral bank (1 million)
  await reward.transfer(bank.address, '1000000000000000000000000');

  // Transfer 100 Mock Tether tokens to investor
  await tether.transfer(accounts[0], '100000000000000000000');
};
