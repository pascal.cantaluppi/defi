const Reward = artifacts.require('Reward');
const Tether = artifacts.require('Tether');
const Bank = artifacts.require('Bank');

async function setupChai() {
  const chai = await import('chai');
  const chaiAsPromised = await import('chai-as-promised');
  chai.use(chaiAsPromised.default);
  chai.should();
}

contract('Bank', ([owner, customer]) => {
  let tether, reward, bank;

  function tokens(number) {
    return web3.utils.toWei(number, 'ether');
  }

  before(async () => {
    // setup chai
    await setupChai();
    // load contracts
    tether = await Tether.new();
    reward = await Reward.new();
    bank = await Bank.new(reward.address, tether.address);

    // transfer all tokens to bank (1 million)
    await reward.transfer(bank.address, tokens('1000000'));

    // transfer 100 mock tethers to customer
    await tether.transfer(customer, tokens('100'), { from: owner });
  });

  describe('Mock tether deployment', async () => {
    it('matches name successfully', async () => {
      const name = await tether.name();
      assert.equal(name, 'Tether');
    });
  });

  describe('Reward token deployment', async () => {
    it('matches name successfully', async () => {
      const name = await reward.name();
      assert.equal(name, 'Reward');
    });
  });

  describe('Decentral bank deployment', async () => {
    it('matches name successfully', async () => {
      const name = await bank.name();
      assert.equal(name, 'Bank');
    });

    it('contract has tokens', async () => {
      let balance = await reward.balanceOf(bank.address);
      assert.equal(balance, tokens('1000000'));
    });

    describe('Yield farming', async () => {
      it('rewards tokens for staking', async () => {
        let result;

        // check investor balance
        result = await tether.balanceOf(customer);
        assert.equal(
          result.toString(),
          tokens('100'),
          'customer mock wallet balance before staking',
        );

        // check staking for customer of 100 tokens
        await tether.approve(bank.address, tokens('100'), { from: customer });
        await bank.depositTokens(tokens('100'), { from: customer });

        // check updated balance of customer
        result = await tether.balanceOf(customer);
        assert.equal(
          result.toString(),
          tokens('0'),
          'customer mock wallet balance after staking 100 tokens',
        );

        // check updated balance of decentral bank
        result = await tether.balanceOf(bank.address);
        assert.equal(
          result.toString(),
          tokens('100'),
          'decentral bank - mock wallet balance after staking from customer',
        );

        // is staking update
        result = await bank.isStaking(customer);
        assert.equal(result.toString(), 'true', 'customer staking status after staking');

        // issue tokens
        await bank.issueTokens({ from: owner });

        // ensure only the owner can issue tokens
        await bank.issueTokens({ from: customer }).should.be.rejected;

        // unstake tokens
        await bank.unstakeTokens({ from: customer });

        // check unstaking balances
        result = await tether.balanceOf(customer);
        assert.equal(
          result.toString(),
          tokens('100'),
          'customer mock wallet balance after unstaking',
        );

        // check updated balance of decentral bank
        result = await tether.balanceOf(bank.address);
        assert.equal(
          result.toString(),
          tokens('0'),
          'decentral bank - mock wallet balance after staking from customer',
        );

        // is staking updated
        result = await bank.isStaking(customer);
        assert.equal(result.toString(), 'false', 'customer is no longer staking after unstaking');
      });
    });
  });
});
