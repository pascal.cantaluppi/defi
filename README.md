# DApp Prototype - DeFi

This platform provides a playground for DeFi (Decentralized Finance) staking and rewards.
<br />The containing methods are used in cryptocurrency and blockchain ecosystems to generate revenue from crypto assets.

<img src="https://gitlab.com/pascal.cantaluppi/defi/-/raw/main/public/img/goliath_grey.png?ref_type=heads"  />

## Setup

The basic setup requires Node.js (LTS 18.18.0), the Yarn package manager and an editor like Visual Studio Code.

### Wallet

Crypto-wallet and gateway to the blockchain app.

https://metamask.io/

### Blockchain

Personal ethereum blockchain.

https://trufflesuite.com/ganache/

&rarr; https://gitlab.com/pascal.cantaluppi/ganache

## Available Scripts

In the project directory, you can run:

### `yarn install`

Installs the dependencies for the project.

### `yarn compile`

Compiles the smart contracts with truffle.

### `yarn test`

Launches the test runner with truffle.

### `yarn migrate:ganache`

Deploys smart contracts to local ganache.

### `yarn migrate:sepolia`

Deploys smart contracts to sepolia testnet.

### `yarn start`

Runs the app in the development mode.

**Ad­di­ti­o­nal**

### `yarn lint`

Runs eslint for static code analysis.

### `yarn format`

Runs prettier for code formatting.
