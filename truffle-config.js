require('dotenv').config();
const HDWalletProvider = require('@truffle/hdwallet-provider');

const privateKey = process.env.REACT_APP_PRIVATE_KEY;
const infura_api_key = process.env.REACT_APP_INFURA_API_KEY;
const etherscan_api_key = process.env.REACT_APP_ETHERSCAN_API_KEY;

module.exports = {
  plugins: ['truffle-plugin-verify'],
  api_keys: {
    etherscan: etherscan_api_key,
  },

  networks: {
    ganache: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '*',
    },
    sepolia: {
      provider: () =>
        new HDWalletProvider(privateKey, `https://sepolia.infura.io/v3/${infura_api_key}`),
      network_id: 11155111, // Sepolia's network ID
      gas: 5000000, // Adjust the gas limit as per your requirements
      gasPrice: 100000000000, // Set the gas price to an appropriate value
      confirmations: 1, // Set the number of confirmations needed for a transaction
      timeoutBlocks: 500, // Set the timeout for transactions
      skipDryRun: false, // Skip the dry run option
    },
  },

  mocha: {
    timeout: 100000,
  },

  contracts_directory: './src/contracts/',
  contracts_build_directory: './src/truffle/',

  compilers: {
    solc: {
      version: '0.8.23',
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
  db: {
    enabled: false,
  },
};
