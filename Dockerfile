FROM node:18.18.0
ENV NODE_ENV development
WORKDIR /app
COPY /package.json .
RUN yarn install
COPY . .
RUN yarn compile
EXPOSE 3000
CMD [ "yarn", "start" ]
