import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardHeader, CardBody, Divider, Image } from '@nextui-org/react';

/**
 * Balance component
 */

export default function Balance(props) {
  return (
    <Card className='max-w-[400px]'>
      <CardHeader className='flex gap-3'>
        <Image alt='Balance' height={40} radius='sm' src='img/staking.png' width={40} />
        <div className='flex flex-col'>
          <p className='text-md'>Current {props.balance} balance</p>
        </div>
      </CardHeader>
      <Divider />
      <CardBody>
        <p>
          0.00{'  '}
          {props.unit}
        </p>
      </CardBody>
      <Divider />
    </Card>
  );
}

Balance.propTypes = {
  balance: PropTypes.node,
  unit: PropTypes.node,
};
