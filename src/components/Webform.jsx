import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Input } from '@nextui-org/react';

/**
 * Webform component
 */

const Webform = (props) => {
  const [amount, setAmount] = useState(0);

  const updateAmount = (e) => {
    //alert(e.target.value);
    setAmount(e.target.value);
  };

  const stake = () => {
    try {
      props.stakeTokens(amount);
    } catch (error) {
      window.alert(error);
    }
  };

  const unstake = () => {
    try {
      props.unstakeTokens();
    } catch (error) {
      window.alert(error);
    }
  };

  return (
    <>
      <div style={{ position: 'absolute', marginTop: '20px' }}>
        <p style={{ marginBottom: '10px' }}>Stake Tokens</p>
        <Input
          key='warning'
          label='USDT'
          type='text'
          color='warning'
          className='max-w-md'
          onChange={updateAmount}
        />
        <Button
          radius='full'
          className='bg-gradient-to-tr from-pink-500 to-yellow-500 text-white shadow-lg'
          style={{ marginTop: '20px', marginBottom: '10px', width: '200px' }}
          onClick={stake}
        >
          Deposit
        </Button>{' '}
        <Button
          radius='full'
          className='bg-gradient-to-tr from-pink-500 to-yellow-500 text-white shadow-lg'
          style={{ marginTop: '20px', marginBottom: '10px', width: '200px' }}
          onClick={unstake}
        >
          Withdraw
        </Button>
      </div>
    </>
  );
};

Webform.propTypes = {
  stakeTokens: PropTypes.node,
  unstakeTokens: PropTypes.node,
};

export default Webform;
