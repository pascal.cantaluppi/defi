import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Code } from '@nextui-org/react';

/**
 * Airdrop component
 */

class Airdrop extends Component {
  constructor() {
    super();
    this.state = { time: {}, seconds: 20 };
    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  startTimer() {
    if (this.timer === 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  countDown() {
    let seconds = this.state.seconds - 1;

    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });

    if (seconds === 0) {
      clearInterval(this.timer);
    }
  }

  secondsToTime(secs) {
    let hours, minutes, seconds;
    hours = Math.floor(secs / (60 * 60));

    let minuteDevisor = secs % (60 * 60);
    minutes = Math.floor(minuteDevisor / 60);

    let secondDevisor = minuteDevisor % 60;
    seconds = Math.ceil(secondDevisor);

    let obj = {
      h: hours,
      m: minutes,
      s: seconds,
    };
    return obj;
  }

  componentDidMount() {
    let timeLeftVar = this.secondsToTime(this.state.seconds);
    this.setState({ time: timeLeftVar });
  }

  airdropReleaseTokens() {
    let balance = this.props.stakingBalance;
    if (balance >= '50000000000000000000') {
      this.startTimer();
    }
  }

  render() {
    this.airdropReleaseTokens();
    return (
      <div>
        <div style={{ position: 'absolute', marginTop: '190px' }}>
          <Code>
            Airdrop: {this.state.time.m}:{this.state.time.s}
          </Code>
        </div>
      </div>
    );
  }
}

Airdrop.propTypes = {
  stakingBalance: PropTypes.node,
};

export default Airdrop;
