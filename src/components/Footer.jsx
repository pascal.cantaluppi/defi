import React from 'react';
import PropTypes from 'prop-types';
import { Chip } from '@nextui-org/react';
import Truffle from './img/truffle.png';

/**
 * Footer component
 */

export default function Footer(props) {
  return (
    <>
      <div style={{ position: 'absolute', marginTop: '240px', marginBottom: '60px' }}>
        <Chip color='warning' variant='faded'>
          Wallet: {props.account}
        </Chip>
      </div>
      <img
        src={Truffle}
        alt='Ganache'
        width='60px'
        style={{ marginLeft: 80, marginTop: 290, marginBottom: 10 }}
      />
      <br />
    </>
  );
}

Footer.propTypes = {
  account: PropTypes.node,
};
