import React from 'react';
import { Spinner } from '@nextui-org/react';

/**
 * Loader component
 */

const Loader = () => {
  return (
    <>
      <div
        style={{
          marginTop: '150px',
          marginLeft: '50px',
        }}
      >
        <div style={{ marginBottom: '20px' }}>Connecting to blockchain..</div>
        <Spinner style={{ marginLeft: '20px', marginBottom: '500px' }} />
        <br />
      </div>
    </>
  );
};

export default Loader;
