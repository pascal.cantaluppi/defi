import React from 'react';
import { Navbar, NavbarBrand, NavbarContent, NavbarItem, Link } from '@nextui-org/react';

/**
 * Header component
 */

const Header = () => {
  return (
    <>
      <Navbar maxWidth={'full'}>
        <NavbarBrand>
          <img
            alt='GOILATH NATIONAL BANK'
            src='/img/goliath.png'
            width='120'
            height='52'
            className='d-inline-block align-top'
          />
        </NavbarBrand>
        <NavbarContent className='hidden sm:flex gap-4' justify='center'>
          <NavbarItem>
            <Link color='foreground' href='#'>
              DeFi DApp - Decentralized Banking
            </Link>
          </NavbarItem>
        </NavbarContent>
        <NavbarContent justify='end'>
          <NavbarItem className='hidden lg:flex'>
            <p style={{ fontSize: 'small' }}>
              {' '}
              Blockchain:{' '}
              <b>
                <Link
                  href='https://sepolia.etherscan.io/'
                  target='_blank'
                  rel='noreferrer'
                  color='warning'
                  size='sm'
                >
                  Sepolia
                </Link>
              </b>
            </p>
          </NavbarItem>
        </NavbarContent>
      </Navbar>
    </>
  );
};

export default Header;
