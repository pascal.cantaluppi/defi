import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, Spacer } from '@nextui-org/react';
import Balance from './Balance';

/**
 * Dashboard component
 */

export default function Dashboard(props) {
  return (
    <>
      <Card>
        <CardBody>
          <p>Yield Staking Account Dashboard</p>
          <div className='flex' style={{ marginTop: '20px' }}>
            <Balance balance='staking' unit='USDT' amount={props.stakingBalance} />
            <Spacer x={6} />
            <Balance balance='reward' unit='RWD' amount={props.rewardBalance} />
          </div>
        </CardBody>
      </Card>
    </>
  );
}

Dashboard.propTypes = {
  stakingBalance: PropTypes.node,
  rewardBalance: PropTypes.node,
};
