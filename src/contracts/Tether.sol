// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.23;

/**
 * @title FFHS Tether
 * @dev Implements a mock version of the Tether token, following ERC-20 standards.
 *      This contract includes the basic functionality of an ERC-20 token such as
 *      transfer and approve operations.
 */
contract Tether {
    // Token metadata: name, symbol, version, and decimals.
    string public name = "Tether";
    string public symbol = "mUSDT";
    uint public constant version = 4;
    uint256 public totalSupply = 1000000000000000000000000; // 1 million tokens with 18 decimal places
    uint8 public decimals = 18;

    // Events for transaction logging
    event Transfer(address indexed _from, address indexed _to, uint _value);
    event Approval(
        address indexed _owner,
        address indexed _spender,
        uint _value
    );

    // Balances and allowances tracked using mappings
    mapping(address => uint256) public balanceOf; // Tracks the balance of each address.
    mapping(address => mapping(address => uint256)) public allowance; // Tracks the allowance of each address to spend tokens on behalf of another.

    /**
     * @dev Constructor sets the total supply of tokens to the address deploying the contract.
     */
    constructor() {
        balanceOf[msg.sender] = totalSupply; // Assigns the total supply to the contract deployer.
    }

    /**
     * @dev Transfers tokens to a specified address.
     * @param _to The recipient's address.
     * @param _value The amount of tokens to transfer.
     * @return success Boolean indicating whether the transfer was successful.
     */
    function transfer(
        address _to,
        uint256 _value
    ) public returns (bool success) {
        require(balanceOf[msg.sender] >= _value, "Insufficient token balance"); // Ensures the sender has enough tokens.
        balanceOf[msg.sender] -= _value; // Deducts the amount from the sender's balance.
        balanceOf[_to] += _value; // Adds the amount to the recipient's balance.
        emit Transfer(msg.sender, _to, _value); // Emits the Transfer event.
        return true;
    }

    /**
     * @dev Approves another address to spend a specified amount of tokens on behalf of msg.sender.
     * @param _spender The address authorized to spend.
     * @param _value The amount of tokens they are authorized to spend.
     * @return success Boolean indicating whether the approval was successful.
     */
    function approve(
        address _spender,
        uint256 _value
    ) public returns (bool success) {
        allowance[msg.sender][_spender] = _value; // Sets the allowance for the spender.
        emit Approval(msg.sender, _spender, _value); // Emits the Approval event.
        return true;
    }

    /**
     * @dev Transfers tokens from one address to another based on a previously set allowance.
     * @param _from The address from which tokens are being transferred.
     * @param _to The recipient's address.
     * @param _value The amount of tokens to transfer.
     * @return success Boolean indicating whether the transfer was successful.
     */
    function transferFrom(
        address _from,
        address _to,
        uint256 _value
    ) public returns (bool success) {
        require(_value <= balanceOf[_from], "Insufficient token balance"); // Checks if the sender has enough tokens.
        require(
            _value <= allowance[_from][msg.sender],
            "Insufficient allowance"
        ); // Checks if the allowance is sufficient.
        balanceOf[_to] += _value; // Adds the amount to the recipient's balance.
        balanceOf[_from] -= _value; // Deducts the amount from the sender's balance.
        allowance[_from][msg.sender] -= _value; // Deducts the amount from the allowance.
        emit Transfer(_from, _to, _value); // Emits the Transfer event.
        return true;
    }
}
