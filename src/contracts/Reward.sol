// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.23;

/**
 * @title FFHS Reward
 * @notice ERC-20 Token Contract for Reward Token
 * @dev This contract implements the ERC-20 standard with basic functionality.
 * @author Pascal Cantaluppi
 */
contract Reward {
    // ERC-20 token details: name, symbol, version, and decimals.
    string public name = "Reward";
    string public symbol = "Reward";
    uint public constant version = 4;
    uint256 public totalSupply = 1000000000000000000000000; // Total supply of 1 million tokens, considering 18 decimal places.
    uint8 public decimals = 18;

    // Events to emit on transactions and approvals.
    event Transfer(address indexed _from, address indexed _to, uint _value);
    event Approval(
        address indexed _owner,
        address indexed _spender,
        uint _value
    );

    // Mappings for tracking balances and allowances.
    mapping(address => uint256) public balanceOf; // Maps each address to its balance.
    mapping(address => mapping(address => uint256)) public allowance; // Maps each address to another address's allowance.

    /**
     * @dev Constructor that initializes the total supply to the creator of the contract.
     */
    constructor() {
        balanceOf[msg.sender] = totalSupply; // Assigning the initial total supply to the contract creator.
    }

    /**
     * @dev Transfers tokens to a specified address.
     * @param _to The address to transfer to.
     * @param _value The amount to be transferred.
     * @return success True if the transfer is successful, false otherwise.
     */
    function transfer(
        address _to,
        uint256 _value
    ) public returns (bool success) {
        require(balanceOf[msg.sender] >= _value, "Insufficient token balance"); // Ensure the sender has enough tokens to transfer.
        balanceOf[msg.sender] -= _value; // Subtract the value from the sender's balance.
        balanceOf[_to] += _value; // Add the value to the recipient's balance.
        emit Transfer(msg.sender, _to, _value); // Emit a Transfer event.
        return true;
    }

    /**
     * @dev Approves another address to spend a specific amount of tokens on behalf of the msg.sender.
     * @param _spender The address authorized to spend.
     * @param _value The amount of tokens they can spend.
     * @return success True if the approval is successful, false otherwise.
     */
    function approve(
        address _spender,
        uint256 _value
    ) public returns (bool success) {
        allowance[msg.sender][_spender] = _value; // Set the spender's allowance to _value.
        emit Approval(msg.sender, _spender, _value); // Emit an Approval event.
        return true;
    }

    /**
     * @dev Transfers tokens from one address to another, based on a previously set allowance.
     * @param _from The address to transfer from.
     * @param _to The address to transfer to.
     * @param _value The amount to be transferred.
     * @return success True if the transfer is successful, false otherwise.
     */
    function transferFrom(
        address _from,
        address _to,
        uint256 _value
    ) public returns (bool success) {
        require(_value <= balanceOf[_from], "Insufficient token balance"); // Ensure _from has enough tokens to transfer.
        require(
            _value <= allowance[_from][msg.sender],
            "Insufficient allowance"
        ); // Check the allowance for this transfer.
        balanceOf[_to] += _value; // Add the value to the recipient's balance.
        balanceOf[_from] -= _value; // Subtract the value from the sender's balance.
        allowance[_from][msg.sender] -= _value; // Deduct the value from the allowance.
        emit Transfer(_from, _to, _value); // Emit a Transfer event.
        return true;
    }
}
