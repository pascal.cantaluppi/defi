// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.23;

/**
 * @title FFHS Migrations
 * @notice Contract for handling contract migrations and upgrades
 * @dev This contract allows the owner to mark migrations as completed and upgrade the contract.
 *      It emits events for completed migrations and contract upgrades.
 * @author Pascal Cantaluppi
 */

contract Migrations {
    // State variable to store the owner of the contract.
    address public owner;

    // State variable to keep track of the last completed migration script.
    uint public last_completed_migration;

    /**
     * @dev Constructor that sets the deployer of the contract as the owner.
     */
    constructor() {
        owner = msg.sender; // Set the deployer of the contract as the owner.
    }

    /**
     * @dev Modifier to restrict function access to only the owner of the contract.
     */
    modifier restricted() {
        // Require that the function caller is the owner of the contract.
        if (msg.sender == owner) _; // Continue execution of the modified function.
    }

    /**
     * @dev Function to update the last completed migration.
     * @param completed The migration number to be marked as completed.
     */
    function setCompleted(uint completed) public restricted {
        last_completed_migration = completed; // Update the last completed migration number.
    }

    /**
     * @dev Function to change the contract address in case of upgrades.
     * @param newAddress The address of the new version of the contract.
     */
    function upgrade(address newAddress) public restricted {
        // Create an instance of the new contract.
        Migrations upgraded = Migrations(newAddress);

        // Set the last completed migration in the new contract.
        upgraded.setCompleted(last_completed_migration);
    }
}
