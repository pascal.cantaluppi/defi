// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.23;

// Importing external contracts: Reward and Tether
import "./Reward.sol";
import "./Tether.sol";

/**
 * @title FFHS Bank
 * @notice A decentralized bank contract that facilitates staking and rewards
 * @dev Users can deposit Tether tokens, stake them, and receive rewards.
 *      Rewards are issued by the owner of the contract.
 *      This contract interacts with the Reward and Tether contracts.
 * @author Pascal Cantaluppi
 */

// Main contract definition for 'Bank'
contract Bank {
    // Public variables
    string public name = "Bank"; // Name of the bank
    uint public constant version = 4; // Contract version
    address public owner; // Owner of the contract
    Tether public tether; // Tether token contract reference
    Reward public reward; // Reward token contract reference

    // Array to keep track of all stakers
    address[] public stakers;

    // Mappings to manage staking details
    mapping(address => uint) public stakingBalance; // Mapping of staking balances
    mapping(address => bool) public hasStaked; // Track if an address has staked
    mapping(address => bool) public isStaking; // Track if an address is currently staking

    /**
     * @dev Constructor sets up the Tether and Reward contracts and initializes the contract owner.
     * @param _reward Address of the Reward contract.
     * @param _tether Address of the Tether contract.
     */
    constructor(Reward _reward, Tether _tether) {
        reward = _reward; // Set the Reward contract
        tether = _tether; // Set the Tether contract
        owner = msg.sender; // Set the owner of the contract to the address that deploys it
    }

    /**
     * @dev Modifier to restrict function access to only the owner of the contract.
     */
    modifier onlyOwner() {
        require(msg.sender == owner, "Only owner can call this function");
        _;
    }

    /**
     * @dev Allows users to deposit Tether tokens and start staking.
     * @param _amount Amount of Tether tokens to stake.
     */
    function depositTokens(uint _amount) public {
        // Require that the staking amount is greater than 0
        require(_amount > 0, "Amount cannot be null");

        // Transfer Tether tokens from the user to the contract
        tether.transferFrom(msg.sender, address(this), _amount);

        // Update the user's staking balance
        stakingBalance[msg.sender] += _amount;

        // Add the user to stakers array only if they haven't staked before
        if (!hasStaked[msg.sender]) {
            stakers.push(msg.sender);
            hasStaked[msg.sender] = true;
        }

        // Mark that the user is staking
        isStaking[msg.sender] = true;
    }

    /**
     * @dev Allows users to unstake their Tether tokens and withdraw them.
     */
    function unstakeTokens() public {
        // Fetch staking balance of the user
        uint balance = stakingBalance[msg.sender];

        // Require that the user has staking balance to unstake
        require(balance > 0, "Staking balance is null");

        // Transfer staked tokens back to the user
        tether.transfer(msg.sender, balance);

        // Reset the staking balance and status
        stakingBalance[msg.sender] = 0;
        isStaking[msg.sender] = false;
    }

    /**
     * @dev Issues rewards to all stakers. Only the owner can call this function.
     */
    function issueTokens() public onlyOwner {
        // Iterate over all stakers
        for (uint i = 0; i < stakers.length; i++) {
            address recipient = stakers[i];
            // Calculate reward as a fraction of staking balance (1/9th)
            uint balance = stakingBalance[recipient] / 9;
            // Issue rewards if balance is positive
            if (balance > 0) {
                reward.transfer(recipient, balance);
            }
        }
    }
}
