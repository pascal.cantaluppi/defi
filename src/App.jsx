import React, { useEffect, useState } from 'react';
import Loader from './components/Loader';
import Header from './components/Header';
import Dashboard from './components/Dashboard';
import Webform from './components/Webform';
import Airdrop from './components/Airdrop';
import Footer from './components/Footer';
import Web3 from 'web3';
import {
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  useDisclosure,
} from '@nextui-org/react';

// Smart Contract ABIs
import Tether from './truffle/Tether.json';
import Reward from './truffle/Reward.json';
import Bank from './truffle/Bank.json';

function App() {
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [error, setError] = useState('');
  const [account, setAccount] = useState('0x0');
  const [tether, setTether] = useState({});
  const [setReward] = useState({});
  const [decentralBank, setDecentralBank] = useState({});
  const [setTetherBalance] = useState('0');
  const [rewardBalance, setRewardBalance] = useState('0');
  const [stakingBalance, setStakingBalance] = useState('0');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const loadWeb3 = async () => {
      try {
        if (window.ethereum) {
          window.web3 = new Web3(window.ethereum);
          await window.ethereum.enable();
        } else {
          //window.alert('Non-ethereum browser detected. You should consider installing a wallet.');
          setError('Non-ethereum browser detected. You should consider installing a wallet.');
          onOpen();
        }
      } catch (error) {
        //window.alert(error);
        console.log(error);
      }
    };

    const loadBlockchainData = async () => {
      try {
        const web3 = window.web3;
        let networkId;

        try {
          const accounts = await web3.eth.getAccounts();
          setAccount(accounts[0]);
          networkId = await web3.eth.net.getId();
        } catch (error) {
          setLoading(false);
          //window.alert(error);
          console.log(error);
          setError(error.toString());
          onOpen();
        }

        // load tether token
        const tetherData = Tether.networks[networkId];
        if (tetherData) {
          const tetherContract = new web3.eth.Contract(Tether.abi, tetherData.address);
          setTether(tetherContract);
          let tetherBalance = await tetherContract.methods.balanceOf(account).call();
          setTetherBalance(tetherBalance.toString());
        } else {
          setError('Tether contract not deployed to network');
          onOpen();
        }

        // load reward token
        const rewardTokenData = Reward.networks[networkId];
        if (rewardTokenData) {
          const rewardContract = new web3.eth.Contract(Reward.abi, rewardTokenData.address);
          setReward(rewardContract);
          let rewardBalance = await rewardContract.methods.balanceOf(account).call();
          setRewardBalance(rewardBalance.toString());
        } else {
          setError('Reward contract not deployed to network');
          onOpen();
        }

        // load decentral bank
        const bankData = Bank.networks[networkId];
        if (bankData) {
          const bankContract = new web3.eth.Contract(Bank.abi, bankData.address);
          setDecentralBank(bankContract);
          let stakingBalance = await bankContract.methods.stakingBalance(account).call();
          setStakingBalance(stakingBalance.toString());
        } else {
          setError('Bank contract not deployed to network');
          onOpen();
        }

        setLoading(false);
      } catch (error) {
        setLoading(false);
        //window.alert(error);
        console.log(error);
        setError(error.toString());
        onOpen();
      }
    };

    const initialize = async () => {
      await loadWeb3();
      await loadBlockchainData();
    };

    initialize();
  }, []);

  const stakeTokens = async (amount) => {
    try {
      setLoading(true);
      let stakeAmount = window.web3.utils.toWei(amount, 'ether');
      await tether.methods.approve(decentralBank._address, stakeAmount).send({ from: account });
      await decentralBank.methods.depositTokens(stakeAmount).send({ from: account });
      setLoading(false);
    } catch (error) {
      setLoading(false);
      //window.alert(error);
      console.log(error);
      setError(error.toString());
      onOpen();
    }
  };

  const unstakeTokens = async () => {
    try {
      setLoading(true);
      await decentralBank.methods.unstakeTokens().send({ from: account });
      setLoading(false);
    } catch (error) {
      setLoading(false);
      //window.alert(error);
      console.log(error);
      setError(error.toString());
      onOpen();
    }
  };

  let content;
  if (loading) {
    content = <Loader />;
  } else {
    content = (
      <>
        <Dashboard stakingBalance={stakingBalance} rewardBalance={rewardBalance} />
        <Webform stakeTokens={stakeTokens} unstakeTokens={unstakeTokens} />
        <Airdrop stakingBalance={stakingBalance} />
        <Footer account={account} />
      </>
    );
  }

  return (
    <div className='dark text-foreground bg-black'>
      <Header />
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        radius='2xl'
        classNames={{
          body: 'py-6',
          backdrop: 'bg-[#292f46]/50 backdrop-opacity-40',
          base: 'border-[#292f46] bg-[#19172c] dark:bg-[#19172c] text-[#a8b0d3]',
          header: 'border-b-[1px] border-[#292f46]',
          footer: 'border-t-[1px] border-[#292f46]',
          closeButton: 'hover:bg-white/5 active:bg-white/10',
        }}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className='flex flex-col gap-1'>Error</ModalHeader>
              <ModalBody>
                <p>{error}</p>
              </ModalBody>
              <ModalFooter>
                <Button color='warning' variant='light' onPress={onClose}>
                  Close
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
      <>{content}</>
    </div>
  );
}

export default App;
