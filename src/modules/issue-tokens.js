const Bank = artifacts.require('Bank');

/* Using the Decentral Bank contract to issue tokens. */
module.exports = async function issueRewards(callback) {
  let bank = await Bank.deployed();
  await bank.issueTokens();
  console.log('Tokens have been issued successfully');
  callback();
};
